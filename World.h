#ifndef WIRTUALYSWIATJPO_WORD_H
#define WIRTUALYSWIATJPO_WORD_H

#include <iostream>
#include <vector>
#include "Organism/Organism.h"
#include "Parameters.h"

class World {
private:
    Organism *world[WORLD_WIDTH][WORLD_HEIGHT];
    int round;
    int livingOrganism;
    bool gameOver;
    std::vector<Organism *> organismList;
    std::vector<Organism *> toDeleteList;
    std::vector<std::string> communicates;
public:
    World();

    void draw(std::ostream &stream);

    friend std::ostream &operator<<(std::ostream &left, World &right);

    void setOrganism(Organism *organism, int x, int y);

    void addOrganism(Organism *organism, int x, int y);

    void unsetOrganism(int x, int y);

    bool colisionOn(Organism *attacker, int x, int y) { return world[x][y]->collision(attacker); }

    Organism* getOrganism(int x, int y){return world[x][y];};

    bool isSet(int x, int y);

    static bool compareOrganism(Organism *left, Organism *right);

    bool getGameStatus();

    void nextRound();

    void addToDelateList(Organism* organism){
        toDeleteList.push_back(organism);
    }
    void addComunique(std::string communique){
        communicates.push_back(communique);
    }
};

#endif //WIRTUALYSWIATJPO_WORD_H