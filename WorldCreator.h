#ifndef WIRTUALYSWIATJPO_WORLDCREATOR_H
#define WIRTUALYSWIATJPO_WORLDCREATOR_H

#include "World.h"

class WorldCreator {
private:
    World *world;
    Coordinate* getRandomCoordinate();
public:
    WorldCreator(World *world) : world(world){};
    void run();

};

#endif //WIRTUALYSWIATJPO_WORLDCREATOR_H