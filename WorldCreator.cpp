#include "WorldCreator.h"
#include "Helpers/Random.h"
#include "Organism/Plant/Grass.h"
#include "Organism/Plant/Thorn.h"
#include "Organism/Plant/Wolfberry.h"
#include "Organism/Animal/Antelope.h"
#include "Organism/Animal/Lion.h"
#include "Organism/Animal/Mosquito.h"
#include "Organism/Animal/Sheep.h"
#include "Organism/Animal/Wolf.h"

void WorldCreator::run() {
    /**
     * Grass
     */
    int randomCount = Random::get(5, 15);
    for(int i = 0; i < randomCount; i++){
        Organism *organism = new Plants::Grass(world, getRandomCoordinate());
        world->addOrganism(organism, organism->getCoordinate()->getWidth(), organism->getCoordinate()->getHeight());
    }
    /**
     * Throw
     */
    randomCount = Random::get(1, 3);
    for(int i = 0; i < randomCount; i++){
        Organism *organism = new Plants::Thorn(world, getRandomCoordinate());
        world->addOrganism(organism, organism->getCoordinate()->getWidth(), organism->getCoordinate()->getHeight());
    }
    /**
     * Wolfberry
     */
    randomCount = Random::get(1, 5);
    for(int i = 0; i < randomCount; i++){
        Organism *organism = new Plants::Wolfberry(world, getRandomCoordinate());
        world->addOrganism(organism, organism->getCoordinate()->getWidth(), organism->getCoordinate()->getHeight());
    }
    /**
     * Antelope
     */
    randomCount = Random::get(1, 5);
    for(int i = 0; i < randomCount; i++){
        Organism *organism = new Animals::Antelope(world, getRandomCoordinate());
        world->addOrganism(organism, organism->getCoordinate()->getWidth(), organism->getCoordinate()->getHeight());
    }
    /**
     * Lion
     */
    randomCount = Random::get(1, 3);
    for(int i = 0; i < randomCount; i++){
        Organism *organism = new Animals::Lion(world, getRandomCoordinate());
        world->addOrganism(organism, organism->getCoordinate()->getWidth(), organism->getCoordinate()->getHeight());
    }
    /**
     * Mosquito
     */
    randomCount = Random::get(2, 5);
    for(int i = 0; i < randomCount; i++){
        Organism *organism = new Animals::Mosquito(world, getRandomCoordinate());
        world->addOrganism(organism, organism->getCoordinate()->getWidth(), organism->getCoordinate()->getHeight());
    }
    /**
     * Sheep
     */
    randomCount = Random::get(2, 5);
    for(int i = 0; i < randomCount; i++){
        Organism *organism = new Animals::Sheep(world, getRandomCoordinate());
        world->addOrganism(organism, organism->getCoordinate()->getWidth(), organism->getCoordinate()->getHeight());
    }
    /**
     * Wolf
     */
    randomCount = Random::get(2, 5);
    for(int i = 0; i < randomCount; i++){
        Organism *organism = new Animals::Wolf(world, getRandomCoordinate());
        world->addOrganism(organism, organism->getCoordinate()->getWidth(), organism->getCoordinate()->getHeight());
    }
}

Coordinate* WorldCreator::getRandomCoordinate() {
    int x, y;
    do{
        x = Random::get(0, (WORLD_WIDTH - 1));
        y = Random::get(0, (WORLD_HEIGHT - 1));
    }while(world->isSet(x, y));

    return new Coordinate(x, y);
}