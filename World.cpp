#include "World.h"

World::World() {
    for (int x = 0; x < WORLD_WIDTH; x++)
        for (int y = 0; y < WORLD_HEIGHT; y++)
            this->world[x][y] = nullptr;

    round = 0;
    livingOrganism = 0;
    gameOver = false;
}

void World::draw(std::ostream &stream) {
    stream << "Tomasz Perkowski 154943\n";
    stream << "Tura: " << round << " Organizmy: " << livingOrganism << "\n";
    for (int y = -1; y <= WORLD_HEIGHT; y++) {
        if (y == -1) {
            stream << ' ';
            for (int i = 0; i < WORLD_WIDTH; i++)
                stream << '_';
            stream << '\n';
        } else if(y < 20) {
            stream << '|';
            for (int x = 0; x < WORLD_WIDTH; x++) {
                if (world[x][y] != NULL)
                    stream << world[x][y]->getChar();
                else
                    stream << ' ';
            }
            stream << "|\n";
        }else{
            stream << '|';
            for (int i = 0; i < WORLD_WIDTH; i++)
                stream << '_';
            stream << "|\n";
        }
    }
    std::reverse(communicates.begin(), communicates.end());
    while(!communicates.empty()){
        stream << communicates.back() << "\n";
        communicates.pop_back();
    }
}

bool World::getGameStatus() {
    return !this->gameOver;
}

std::ostream &operator<<(std::ostream &stream, World &world) {
    world.draw(stream);
    return stream;
}

void World::addOrganism(Organism *organism, int x, int y) {
    livingOrganism++;
    this->world[x][y] = organism;
}

void World::setOrganism(Organism *organism, int x, int y) {
    this->world[x][y] = organism;
}

void World::unsetOrganism(int x, int y) {
    world[x][y] = nullptr;
}

void World::nextRound() {
    round++;
    for (int x = 0; x < WORLD_WIDTH; x++) {
        for (int y = 0; y < WORLD_HEIGHT; y++) {
            if (world[x][y] != nullptr) {
                organismList.push_back(world[x][y]);
            }
        }
    }
    std::sort(organismList.begin(), organismList.end(), compareOrganism);
    Organism *temp;
    while(!organismList.empty()){
        temp = organismList.back();
        /**
         * Sprawdzamy czy jeszcze żyje
         */
        if(temp == world[temp->getCoordinate()->getWidth()][temp->getCoordinate()->getHeight()]){
            temp->incAge();
            temp->move();
        }
        organismList.pop_back();
    }
    while(!toDeleteList.empty()){
        temp = toDeleteList.back();
        livingOrganism--;
        delete temp;
        toDeleteList.pop_back();
    }

}
bool World::compareOrganism(Organism *left, Organism *right) {
    if(left->getInitiative() < right->getInitiative()){
        return true;
    }else if(left->getInitiative() > right->getInitiative()){
        return false;
    }
    return (left->getAge() < right->getAge());
}

bool World::isSet(int x, int y) {
    return world[x][y] != nullptr;
}