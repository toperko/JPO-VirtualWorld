#ifndef WIRTUALYSWIATJPO_ORGANISM_H
#define WIRTUALYSWIATJPO_ORGANISM_H

#include <string>
#include "../Helpers/Coordinate.h"
#include "../Helpers/Directions.h"

class World;

class Organism {
protected:
    World* world;
    Coordinate* coordinate;
    int strength;
    int initiative;
    int age;
    Directions getActionDirection();
public:
    Organism(World* world, Coordinate* coordinate, int strength, int initiative);
    Organism(const Organism &original);
    ~Organism();
    virtual void move() = 0;
    virtual bool collision(Organism* attacker) = 0;
    virtual char getChar() = 0;
    int getStrength(){ return strength; };
    int getInitiative(){ return initiative; };
    int getAge(){ return age; };
    void incAge(){ age++; };
    bool operator>(const Organism &opponent);
    Coordinate* getCoordinate(){ return coordinate;}
    virtual std::string getName() = 0;

};

#endif //WIRTUALYSWIATJPO_ORGANISM_H