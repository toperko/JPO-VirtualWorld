#include "Organism.h"
#include "../Helpers/Random.h"

Organism::Organism(World* world, Coordinate* coordinate, int strength, int initiative) {
    this->world = world;
    this->coordinate = coordinate;
    this->strength = strength;
    this->initiative = initiative;
    this->age = 0;
}

Organism::Organism(const Organism &original) {
    this->world = original.world;
    this->coordinate = new Coordinate(*original.coordinate);
    this->strength = original.strength;
    this->initiative = original.initiative;
    this->age = 0;
}
Organism::~Organism() {
    delete coordinate;
}

/**
 * Porównuje Atakującego z przeciwnikiem
 *
 * @param opponent
 * @return
 */
bool Organism::operator>(const Organism &opponent) {
    return this->strength >= opponent.strength;
}

Directions Organism::getActionDirection() {
    Directions i = static_cast<Directions>(Random::get(0, 3));
    return i;
}
