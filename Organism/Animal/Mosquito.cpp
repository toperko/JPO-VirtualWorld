#include "Mosquito.h"
#include "../../World.h"
#include "../../Helpers/Random.h"

using namespace Animals;

int Mosquito::getCompanion() {
    int x, y, s = 0;
    s = strength;
    x = getCoordinate()->getWidth();
    y = getCoordinate()->getHeight();
    if(world->isSet(x + 1, y) && world->getOrganism(x + 1, y)->getChar() == getChar()){
        s++;
    }
    if(world->isSet(x, y + 1) && world->getOrganism(x, y + 1)->getChar() == getChar()){
        s++;
    }
    if(world->isSet(x - 1, y) && world->getOrganism(x - 1, y)->getChar() == getChar()){
        s++;
    }
    if(world->isSet(x, y - 1) && world->getOrganism(x, y - 1)->getChar() == getChar()){
        s++;
    }
    return s;
}

void Mosquito::move() {
    *oldCordinate = *coordinate;
    int x, y;
    do {
        x = this->coordinate->getWidth();
        y = this->coordinate->getHeight();
        switch (this->getAttackDirection()) {
            case Directions::up :
                y -= 1;
                break;
            case Directions::right :
                x += 1;
                break;
            case Directions::down :
                y += 1;
                break;
            case Directions::left :
                x -= 1;
                break;
        }
    } while (!Coordinate::chceckCordinate(x, y));

    if(world->isSet(x, y)){
        world->addComunique("kolizja");
        if(world->colisionOn(this, x, y)){
            world->unsetOrganism(getCoordinate()->getWidth(), getCoordinate()->getHeight());
            getCoordinate()->setWidth(x);
            getCoordinate()->setHeight(y);
            world->setOrganism(this, x, y);
        }else{
            if(Random::get(0, 1) == 1){
                world->addComunique(getName() + " ratuje się");
                return;
            }
            world->addComunique("Komarowi nie udalo sie uciec");
            world->unsetOrganism(getCoordinate()->getWidth(), getCoordinate()->getHeight());
            world->addToDelateList(this);
        }
        return;
    }
    world->unsetOrganism(this->coordinate->getWidth(), this->coordinate->getHeight());
    this->coordinate->setWidth(x);
    this->coordinate->setHeight(y);
    world->setOrganism(this, x, y);
    world->addComunique(getName() + " przesuwa sie");
}

bool Mosquito::collision(Organism *attacker) {
    world->addComunique(attacker->getName() + " atakuje " + getName());
    if(attacker->getStrength() > this->getStrength()){
        world->addComunique(attacker->getName() + " zwycieza ");

        if(Random::get(0, 1) == 1){
            world->addComunique(getName() + " ratuje się");
            *coordinate = *oldCordinate;
            world->setOrganism(this, getCoordinate()->getWidth(), getCoordinate()->getHeight());
        }else{
            world->addComunique("Komarowi nie udalo sie uciec");
            world->addToDelateList(this);
        }

        return true;
    }else{
        world->addComunique(getName() + " zwycieza ");
        return false;
    }
}

