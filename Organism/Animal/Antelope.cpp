#include "Antelope.h"
#include "../../World.h"
#include "../../Helpers/Random.h"

using namespace Animals;

void Antelope::move() {
    int x, y;
    do {
        x = this->coordinate->getWidth();
        y = this->coordinate->getHeight();
        switch (this->getAttackDirection()) {
            case Directions::up :
                y -= 2;
                break;
            case Directions::right :
                x += 2;
                break;
            case Directions::down :
                y += 2;
                break;
            case Directions::left :
                x -= 2;
                break;
        }
    } while (!Coordinate::chceckCordinate(x, y));

    if (world->isSet(x, y)) {
        world->addComunique("kolizja");
        if(world->colisionOn(this, x, y)){
            world->unsetOrganism(getCoordinate()->getWidth(), getCoordinate()->getHeight());
            getCoordinate()->setWidth(x);
            getCoordinate()->setHeight(y);
            world->setOrganism(this, x, y);
        }else{
            world->unsetOrganism(getCoordinate()->getWidth(), getCoordinate()->getHeight());
            world->addToDelateList(this);
        }
        return;
    }
    world->unsetOrganism(this->coordinate->getWidth(), this->coordinate->getHeight());
    this->coordinate->setWidth(x);
    this->coordinate->setHeight(y);
    world->setOrganism(this, x, y);
    world->addComunique(getName() + " przesuwa sie");
}

bool Antelope::collision(Organism *attacker) {
    if (Random::get(0, 1) == 0) {
        world->addComunique(getName() + " nie ucieka przed " + attacker->getName());
        return Animal::collision(attacker);
    }

    int x, y;
    x = coordinate->getWidth();
    y = coordinate->getHeight();
    if (!world->isSet(x + 1, y)) {
        x += 1;
    } else if (!world->isSet(x, y - 1)) {
        y -= 1;
    } else if (!world->isSet(x - 1, y)) {
        x -= 1;
    } else if (!world->isSet(x, y + 1)) {
        y += 1;
    } else {
        world->addComunique(getName() + " nie ma gdzie uciec przed "+ attacker->getName());

        return Animal::collision(attacker);
    }
    world->addComunique(getName() + " ucieka przed " + attacker->getName());

    world->unsetOrganism(this->coordinate->getWidth(), this->coordinate->getHeight());
    this->coordinate->setWidth(x);
    this->coordinate->setHeight(y);
    world->setOrganism(this, x, y);

    return true;
}