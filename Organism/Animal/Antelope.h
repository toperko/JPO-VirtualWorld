#ifndef WIRTUALYSWIATJPO_ANTELOPE_H
#define WIRTUALYSWIATJPO_ANTELOPE_H

#include <iostream>
#include "Animal.h"
#include "../../Parameters.h"

namespace Animals {
    /**
     * Akcja : Zasięg ruchu wynosi 2 pola.
     * kolizja : 50% szans na ucieczkę przed walką. Wówczas przesuwa się na niezajęte sąsiednie pole.
     */
    class Antelope : public Animal {
    public:
        Antelope(World *world, Coordinate *coordinate) : Animal(world, coordinate, ANTELOPE_STRENGTH, ANTELOPE_INITIATIVE) {};
        ~Antelope(){Animal::~Animal();};
        char getChar() { return 'A'; }
        void move();
        bool collision(Organism* attacker);
        std::string getName(){ return std::string("Antylopa");};
    };
};

#endif //WIRTUALYSWIATJPO_ANTELOPE_H