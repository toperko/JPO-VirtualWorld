#include "Lion.h"
#include "../../World.h"
using namespace Animals;


bool Lion::collision(Organism *attacker) {
    if(attacker->getStrength() < 5){
        world->addComunique(attacker->getName() + " poddaje sie przed lwem ");
        return false;
    }
    return Animal::collision(attacker);
}