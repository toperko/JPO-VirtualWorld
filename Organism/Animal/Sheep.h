#ifndef WIRTUALYSWIATJPO_SHEEP_H
#define WIRTUALYSWIATJPO_SHEEP_H

#include <iostream>
#include "Animal.h"
#include "../../Parameters.h"

namespace Animals {
    class Sheep : public Animal {
    public:
        Sheep(World *world, Coordinate *coordinate) : Animal(world, coordinate, SHEEP_STRENGTH, SHEEP_INITIATIVE) {};
        ~Sheep(){Animal::~Animal();};

        char getChar() { return 'O'; }
        std::string getName(){ return std::string("Owca");};
    };
}
#endif //WIRTUALYSWIATJPO_SHEEP_H
