#ifndef WIRTUALYSWIATJPO_ANIMAL_H
#define WIRTUALYSWIATJPO_ANIMAL_H

#include "../Organism.h"

class Animal : public Organism {
protected:


    Directions getAttackDirection();

public:
    Animal(World *world, Coordinate *coordinate, int strength, int initiative) : Organism(world, coordinate, strength, initiative) {};
    ~Animal(){Organism::~Organism();};
    void move();

    bool collision(Organism* attacker);
};

#endif //WIRTUALYSWIATJPO_ANIMAL_H