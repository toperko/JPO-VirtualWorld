#ifndef WIRTUALYSWIATJPO_MOSQUITO_H
#define WIRTUALYSWIATJPO_MOSQUITO_H

#include <iostream>
#include "Animal.h"
#include "../../Parameters.h"

namespace Animals {
    /**
    * Akcja : +1 do inicjatywy i +1 do siły za każdego sąsiadującego komara
    * kolizja : Jeśli zostanie pokonany, ma 50% szans na przeżycie (wraca na poprzednie pole).
    */
    class Mosquito : public Animal {
    private:
        int getCompanion();
        Coordinate* oldCordinate;
    public:
        Mosquito(World *world, Coordinate *coordinate) : Animal(world, coordinate, MOSQUITO_STRENGTH, MOSQUITO_INITIATIVE) {
            oldCordinate = new Coordinate(*coordinate);
        };
        ~Mosquito(){delete oldCordinate; Animal::~Animal(); };
        char getChar() { return 'K'; }
        void move();
        bool collision(Organism *attacker);

        int getStrength(){return strength + getCompanion();};
        int getInitiative(){return initiative + getCompanion();};
        std::string getName(){ return std::string("Komar");};
    };
}
#endif //WIRTUALYSWIATJPO_MOSQUITO_H