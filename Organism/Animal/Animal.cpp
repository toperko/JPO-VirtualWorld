#include "Animal.h"
#include "../../World.h"


void Animal::move() {
    int x, y;
    do {
        x = this->coordinate->getWidth();
        y = this->coordinate->getHeight();
        switch (this->getAttackDirection()) {
            case Directions::up :
                y -= 1;
                break;
            case Directions::right :
                x += 1;
                break;
            case Directions::down :
                y += 1;
                break;
            case Directions::left :
                x -= 1;
                break;
        }
    } while (!Coordinate::chceckCordinate(x, y));

    if(world->isSet(x, y)){
        world->addComunique("kolizja");
        if(world->colisionOn(this, x, y)){
            world->unsetOrganism(getCoordinate()->getWidth(), getCoordinate()->getHeight());
            getCoordinate()->setWidth(x);
            getCoordinate()->setHeight(y);
            world->setOrganism(this, x, y);
        }else{
            world->unsetOrganism(getCoordinate()->getWidth(), getCoordinate()->getHeight());
            world->addToDelateList(this);
        }
        return;
    }
    world->unsetOrganism(this->coordinate->getWidth(), this->coordinate->getHeight());
    this->coordinate->setWidth(x);
    this->coordinate->setHeight(y);
    world->setOrganism(this, x, y);
    world->addComunique(getName() + " przesuwa sie");
}

bool Animal::collision(Organism *attacker) {
    world->addComunique(attacker->getName() + " atakuje " + getName());
    if(attacker->getStrength() > this->getStrength()){
        world->addToDelateList(this);
        world->addComunique(attacker->getName() + " zwycieza ");
        return true;
    }else{
        world->addComunique(getName() + " zwycieza ");
        return false;
    }
}

Directions Animal::getAttackDirection() {
    return getActionDirection();
}