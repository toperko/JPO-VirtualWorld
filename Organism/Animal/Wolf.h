#ifndef WIRTUALYSWIATJPO_WOLF_H
#define WIRTUALYSWIATJPO_WOLF_H

#include <iostream>
#include "Animal.h"
#include "../../Parameters.h"

namespace Animals {
    class Wolf : public Animal {
    public:
        Wolf(World *world, Coordinate *coordinate) : Animal(world, coordinate, WOLF_STRENGTH, WOLF_INITIATIVE) {};
        ~Wolf(){Animal::~Animal();};

        char getChar() { return 'W'; }
        std::string getName(){ return std::string("Wilk");};
    };
}
#endif //WIRTUALYSWIATJPO_WOLF_H