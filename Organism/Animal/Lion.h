#ifndef WIRTUALYSWIATJPO_LION_H
#define WIRTUALYSWIATJPO_LION_H

#include <iostream>
#include "Animal.h"
#include "../../Parameters.h"

namespace Animals {
    class Lion : public Animal {
    public:
        Lion(World *world, Coordinate *coordinate) : Animal(world, coordinate, LION_STRENGTH, LION_INITIATIVE) {};
        ~Lion(){Animal::~Animal();};
        char getChar() { return 'L'; }
        std::string getName(){ return std::string("Lew");};
        bool collision(Organism* attacker);
    };
}
#endif //WIRTUALYSWIATJPO_LION_H
