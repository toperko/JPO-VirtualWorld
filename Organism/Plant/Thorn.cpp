#include "Thorn.h"
#include "../../World.h"
using namespace Plants;

int Thorn::getGrowthChance() {
    return THORN_GROWTH_CHANCE;
}

Organism* Thorn::newOrganism() {
    return new Thorn(*this);
}

bool Thorn::collision(Organism *attacker) {
    world->addComunique(attacker->getName() + " atakuje " + getName());
    if(attacker > this){
        world->addToDelateList(this);
        world->addComunique(attacker->getName() + " zwycieza ");
        return true;
    }else{
        world->addComunique(this->getName() + " zwycieza ");
        return false;
    }
}

void Thorn::move() {
    if(!isGrowth()){
        return;
    }

    int x, y, i = 0;
    do{
        x = this->coordinate->getWidth();
        y = this->coordinate->getHeight();
        switch (this->getActionDirection()) {
            case Directions::up :
                y -= 1;
                break;
            case Directions::right :
                x += 1;
                break;
            case Directions::down :
                y += 1;
                break;
            case Directions::left :
                x -= 1;
                break;
        }
    }while(
            ++i < 4 //ograniczenie ilosci prob
            && !(
                    Coordinate::chceckCordinate(x, y)
                    && ( !world->isSet(x, y) || world->getOrganism(x, y)->getChar() != getChar()  ) //ograniczenie kanibalizmu
                )
            );
    if(!Coordinate::chceckCordinate(x, y) || (world->isSet(x, y) &&  world->getOrganism(x, y)->getChar() == getChar() )){
        return;
    }
    if(world->isSet(x, y)){
        Organism* victim = world->getOrganism(x, y);
        world->unsetOrganism(x, y);
        world->addToDelateList(victim);
        Organism *newOrganism = this->newOrganism();
        newOrganism->getCoordinate()->setWidth(x);
        newOrganism->getCoordinate()->setHeight(y);
        world->addOrganism(newOrganism, x, y);
        world->addComunique("Ciernie rozrastają sie i zabijaja " + victim->getName());
    }else{
        Organism *newOrganism = this->newOrganism();
        newOrganism->getCoordinate()->setWidth(x);
        newOrganism->getCoordinate()->setHeight(y);
        world->addOrganism(newOrganism, x, y);
        world->addComunique("Ciernie rozrastają sie");
    }

}