#ifndef WIRTUALYSWIATJPO_WOLFBERRY_H
#define WIRTUALYSWIATJPO_WOLFBERRY_H

#include <iostream>
#include "Plant.h"
#include "../../Parameters.h"

namespace Plants {
    /**
     * Zwierze, które zjadło tę roślinę, ginie.
     */
    class Wolfberry : public Plant {
    private:
        int getGrowthChance();
    public:
        bool collision(Organism* attacker);

        char getChar() { return 'J'; }
        Organism* newOrganism();
        Wolfberry(World *world, Coordinate *coordinate) : Plant(world, coordinate, WOLFBERRY_STRENGTH){};

        std::string getName(){ return std::string("Wilcze Jagody");};
    };
}
#endif //WIRTUALYSWIATJPO_WOLFBERRY_H