#include <random>
#include "Plant.h"
#include "../../World.h"
void Plant::move() {

    if(!isGrowth()){
        return;
    }

    int x, y, i = 0;
    do{
        x = this->coordinate->getWidth();
        y = this->coordinate->getHeight();
        switch (this->getActionDirection()) {
            case Directions::up :
                y -= 1;
                break;
            case Directions::right :
                x += 1;
                break;
            case Directions::down :
                y += 1;
                break;
            case Directions::left :
                x -= 1;
                break;
        }
    }while(++i < 4 && !(Coordinate::chceckCordinate(x, y) && !world->isSet(x, y))  );
    if(!Coordinate::chceckCordinate(x, y) || world->isSet(x, y)){
        return;
    }
    Organism *newOrganism = this->newOrganism();
    newOrganism->getCoordinate()->setWidth(x);
    newOrganism->getCoordinate()->setHeight(y);
    world->addOrganism(newOrganism, x, y);
    world->addComunique(getName() + " zostala zasiana");
}

bool Plant::isGrowth() {
    /**
     * C++11 Random
     */
    std::random_device randomDevice;
    std::mt19937 randomGeneratorEngine(randomDevice());
    std::uniform_int_distribution<> randomDistributor(0, 100);
    return (randomDistributor(randomGeneratorEngine) < getGrowthChance());
}

bool Plant::collision(Organism* attacker) {
    world->addToDelateList(this);
    world->addComunique(getName() + " zostala zjedzona przez " + attacker->getName());
    return true;
};