#include "Wolfberry.h"
#include "../../World.h"
using namespace Plants;


int Wolfberry::getGrowthChance() {
    return WOLFBERRY_GROWTH_CHANCE;
}

Organism* Wolfberry::newOrganism() {
    return new Wolfberry(*this);
}

bool Wolfberry::collision(Organism *attacker) {
    world->unsetOrganism(this->getCoordinate()->getWidth(), this->getCoordinate()->getHeight());
    world->addComunique(getName() + " zostala zjedzona przez " + attacker->getName());
    world->addComunique(attacker->getName() + " ginie od Wilczych Jagod");
    world->addToDelateList(this);

    return false;
}