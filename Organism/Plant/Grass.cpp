#include "Grass.h"
using namespace Plants;

int Grass::getGrowthChance() {
    return GRASS_GROWTH_CHANCE;
}

Organism* Grass::newOrganism() {
    return new Grass(*this);
}