#ifndef WIRTUALYSWIATJPO_PLANT_H
#define WIRTUALYSWIATJPO_PLANT_H

#include "../Organism.h"

class Plant : public Organism {
protected:
    bool isGrowth();
public:
    Plant(World *world, Coordinate* coordinate, int strength) : Organism(world, coordinate, strength, 0){};
    ~Plant(){Organism::~Organism();};
    virtual Organism *newOrganism() = 0;
    void move();

    virtual int getGrowthChance() = 0;
    bool collision(Organism* attacker);
};

#endif //WIRTUALYSWIATJPO_PLANT_H