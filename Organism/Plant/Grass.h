#ifndef WIRTUALYSWIATJPO_GRASS_H
#define WIRTUALYSWIATJPO_GRASS_H

#include <iostream>
#include "Plant.h"
#include "../../Parameters.h"

namespace Plants {
    class Grass : public Plant {
    private:
        int getGrowthChance();
    public:
        Grass(World *world, Coordinate *coordinate) : Plant(world, coordinate, GRASS_STRENGTH){};
        char getChar() { return 'T'; }
        Organism* newOrganism();
        std::string getName(){ return std::string("Trawa");};
    };
}


#endif //WIRTUALYSWIATJPO_GRASS_H