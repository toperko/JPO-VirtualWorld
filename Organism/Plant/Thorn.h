#ifndef WIRTUALYSWIATJPO_THORN_H
#define WIRTUALYSWIATJPO_THORN_H


#include <iostream>
#include "Plant.h"
#include "../../Parameters.h"

namespace Plants {
    /**
     * Akcja: Próby rozprzestrzeniania się zawsze kończą się sukcesem.
     */
    class Thorn : public Plant {
    private:
        int getGrowthChance();
    public:
        Thorn(World *world, Coordinate *coordinate) : Plant(world, coordinate, THORN_STRENGTH){};
        bool collision(Organism* attacker);
        char getChar() { return 'C'; }
        void move();
        Organism* newOrganism();
        std::string getName(){ return std::string("Ciernie");};
    };
}

#endif //WIRTUALYSWIATJPO_THORN_H
