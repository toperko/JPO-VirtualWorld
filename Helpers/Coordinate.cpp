#include "Coordinate.h"
#include "../Parameters.h"
#include "CoordinateException.h"

Coordinate::Coordinate() {
    setHeight(0);
    setWidth(0);
}

Coordinate::Coordinate(int width, int height) {
    setHeight(width);
    setWidth(height);
}

Coordinate::Coordinate(const Coordinate &orginal) {
    setHeight(orginal.height);
    setWidth(orginal.width);
}

Coordinate Coordinate::operator=(const Coordinate &right) {
    setHeight(right.height);
    setWidth(right.width);
}

/**
 * @param width Szeroksc
 * @throws CoordinateException
 */
void Coordinate::setWidth(int width) {
    if(width >= WORLD_WIDTH || width < 0){
        throw CoordinateException("Width outside the map");
    }
    this->width = width;
}
/**
 * @return Szerokosc
 */
int Coordinate::getWidth() {
    return this->width;
}
/**
 * @param Wysokosc
 * @throws CoordinateException
 */
void Coordinate::setHeight(int height) {
    if(height >= WORLD_HEIGHT || height < 0){
        throw CoordinateException("Height outside the map");
    }
    this->height = height;
}
/**
 * @return Wysokosc
 */
int Coordinate::getHeight() {
    return this->height;
}

bool Coordinate::chceckCordinate(int width, int height) {
    return (width >= 0) && (height >= 0) && (width < WORLD_WIDTH) && (height < WORLD_HEIGHT);
}

bool Coordinate::chceckCordinate(Directions direction, int width, int height) {
    switch (direction) {
        case Directions::up :
            height -= 1;
            break;
        case Directions::right :
            width += 1;
            break;
        case Directions::down :
            height += 1;
            break;
        case Directions::left :
            width -= 1;
            break;
    }
    return Coordinate::chceckCordinate(width, height);
}