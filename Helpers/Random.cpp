#include <random>
#include "Random.h"

int Random::get(int min, int max) {
    std::random_device randomDevice;
    std::mt19937 randomGeneratorEngine(randomDevice());
    std::uniform_int_distribution<> randomDistributor(min, max);
    return randomDistributor(randomGeneratorEngine);
}