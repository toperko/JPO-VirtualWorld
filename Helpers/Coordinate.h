#ifndef WIRTUALYSWIATJPO_COORDINATE_H
#define WIRTUALYSWIATJPO_COORDINATE_H

#include "Directions.h"

class Coordinate {
private:
    int width;
    int height;
public:
    Coordinate();
    Coordinate(const Coordinate &orginal);
    Coordinate(int width, int height);
    Coordinate operator=(const Coordinate &right);
    void setWidth(int width);
    int getWidth();
    void setHeight(int height);
    int getHeight();

    static bool chceckCordinate(int width, int height);
    static bool chceckCordinate(Directions direction,int width, int height);
};

#endif //WIRTUALYSWIATJPO_COORDINATE_H