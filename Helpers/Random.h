//
// Created by Tomasz Perkowski on 20.01.2018.
//

#ifndef WIRTUALYSWIATJPO_RANDOM_H
#define WIRTUALYSWIATJPO_RANDOM_H


class Random {
public:
    static int get(int min, int max);
};


#endif //WIRTUALYSWIATJPO_RANDOM_H
