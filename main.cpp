#include "World.h"
#include "Organism/Plant/Grass.h"
#include "Organism/Animal/Lion.h"
#include "WorldCreator.h"

int main() {
    World *world = new World();

    WorldCreator *worldCreator = new WorldCreator(world);
    worldCreator->run();

    char key;
    while (world->getGameStatus()) {
        std::system("clear");
        std::cout << *world;
        do {
            std::cin >> key;
        } while (key != 'c');
        world->nextRound();
    }

    return 0;
}